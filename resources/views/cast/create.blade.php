@extends('admin.master')

@section('content')
<div class="mr-4 ml-4 mt-3">
    <div class="card ">
        <div class="card-header">
            <h3 class="card-title">Tambah Data Cast</h3>
        </div>

        <form role="form" action="{{ url('cast') }}" method="POST">
            @csrf

            <div class="card-body">
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama') }}" placeholder="Masukan Nama"><br>
                    @error('nama')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="number" class="form-control" id="umur" name="umur" value="{{ old('umur') }}" placeholder="Masukan Umur"><br>
                    @error('umur')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="bio">Bio</label>
                    <input type="text" class="form-control" id="bio" name="bio" value="{{ old('bio') }}" placeholder="Masukan Bio"><br>
                    @error('bio')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan Data</button>
            </div>
            </>
    </div>
</div>
@endsection