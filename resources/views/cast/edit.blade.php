@extends('admin.master')

@section('content')
<div class="mr-4 ml-4 mt-3">
    <div class="card ">
        <div class="card-header">
            <h3 class="card-title">Edit Data Cast {{$caste->id}}</h3>
        </div>

        <form role="form" action="/cast/{{$caste->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', $caste->nama) }}" placeholder="Masukan Nama"><br>
                    @error('nama')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="number" class="form-control" id="umur" name="umur" value="{{ old('umur', $caste->umur) }}" placeholder="Masukan Umur"><br>
                    @error('umur')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="bio">Bio</label>
                    <input type="text" class="form-control" id="bio" name="bio" value="{{ old('bio', $caste->bio) }}" placeholder="Masukan Bio"><br>
                    @error('bio')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan Data</button>
            </div>
            </>
    </div>
</div>
@endsection