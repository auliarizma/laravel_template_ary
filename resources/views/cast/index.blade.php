@extends('admin.master')

@section('content')
<div class="mt-3 mr-4 ml-4">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title text-bold mr-5">Data Cast</h3>
        </div>
        <!-- /.card-header -->

        <div class="card-body">
            @if(session('success'))
            <div class="alert alert-default-success">{{session('success')}}</div>
            @endif
            <table id="example1" class="table table-bordered table-striped">
                <a href="{{ url('cast/create') }}"><button class="btn btn-primary mb-3 mr-5">Tambah</button></a>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Umur</th>
                        <th>Bio</th>
                        <th>Opsi</th>
                    </tr>
                </thead>
                <tbody>

                    @forelse($cast as $key=>$caste)
                    <tr>
                        <td>{{ $key+1}}</td>
                        <td>{{ $caste->nama}}</td>
                        <td>{{ $caste->umur}}</td>
                        <td>{{ $caste->bio}}</td>
                        <td style="display: flex;">
                            <a href="/cast/{{$caste->id}}" class="btn btn-info btn-sm">Info</a>|
                            <a href="/cast/{{$caste->id}}/edit" class="btn btn-warning btn-sm">Edit</a>|
                            <form action="/cast/{{$caste->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger btn-sm">
                            </form>
                        </td>


                    </tr>
                    @empty
                    <tr>
                        <td colspan="4" align="center">Belum Ada Data</td>
                    </tr>
                    @endforelse
            </table>
        </div>
    </div>
</div>
<!-- /.card-body -->
</div>
@endsection

@push('scripts')
<script src="{{ asset('/admin/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
    $(function() {
        $("#example1").DataTable();
    });
</script>
@endpush