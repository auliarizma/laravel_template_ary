<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('tabel.index');
});
//Route::get('/table', function () {
// return view('tabel.table');
//});
//Route::get('/table-data', function () {
//return view('tabel.table-data');
//});
//Route::get('/master', function () {
// return view('admin.master');
//});

//Route::get('/create', function () {
//  return view('cast.create');
//});

//Route::get('/home', 'RegisterController@home');
Route::get('/cast', 'CastController@index');
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
Route::get('/cast/{Id}', 'CastController@show');
Route::get('/cast/{Id}/edit', 'CastController@edit');
Route::put('/cast/{Id}', 'CastController@update');
Route::delete('/cast/{Id}', 'CastController@destroy');
