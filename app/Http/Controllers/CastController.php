<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('cast.index', compact('cast'));
    }

    public function create()
    {
        return view('cast.create');
    }
    public function store(Request $request)
    {

        // dd($request->all());
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required|numeric',
            'bio' => 'required',

        ]);

        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]

        ]);
        return redirect('/cast')->with('success', 'Data Berhasil Disimpan');
    }
    public function  show($id)
    {
        $caste = DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('caste'));
    }
    public function  edit($id)
    {
        $caste = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('caste'));
    }

    public function  update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required|',
            'umur' => 'required|numeric',
            'bio' => 'required',

        ]);

        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']

            ]);

        return redirect('/cast')->with('success', 'Data Berhasil Di Rubah');
    }
    public function destroy($id)
    {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast')->with('success', 'Data Berhasil Di Hapus');;
    }
}
